__programmer__ = 'Eniac'
__date__ = '2020/3/22 11:36 '
# -------------------------------
# FIND ALL 9 BUGS
# NO EXTER EXERCISE
# -------------------------------

import pygame #bug here
from pygame import image,Rect,Surface  # bug

TILE_POSITIONS = [
    ('#',  0, 0),  # wall
    ('face', 3, 0),# face
    ('.',  2, 0),  # dot
    (' ', 0, 1),  # floor
]

SIZE = 32


def load_tiles():
    '''
    Load tiles from an image file into a dictionary
    Returns a tuple of (image, tile_dict)
    '''
    tiles = {}
    tile_img = pygame.image.load('tiles.xpm')
    for symbol, x, y in TILE_POSITIONS:
        # tiles[symbol] = pygame.Rect(x*SIZE, y*SIZE, SIZE, SIZE)
        rect = pygame.Rect(x * SIZE, y * SIZE, SIZE, SIZE)
        tiles[symbol] = rect
    return tile_img, tiles

def get_tiles_rect(x,y):
    return x*SIZE,y*SIZE

if __name__ == '__main__':
    tile_img, tiles = load_tiles()

    m = pygame.Surface((128,32))
    m.blit(tile_img,  get_tiles_rect(0, 0), tiles['#'])
    m.blit(tile_img,  get_tiles_rect(1, 0), tiles[' '])
    m.blit(tile_img,  get_tiles_rect(2, 0), tiles['face'])
    m.blit(tile_img,  get_tiles_rect(3, 0), tiles['.'])

    pygame.image.save(m,'tile_combo.png')

    # ------------------------------------

    # Optional exercise:
    # make the print satatemnet below work
    # by modifying the class
    # so that it prints the char attribute